﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MITT_Leni_Nurliani.IServices;
using MITT_Leni_Nurliani.Models;
using MITT_Leni_Nurliani.Services;
using System.Collections.Generic;

namespace MITT_Leni_Nurliani.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserSkillsController : ControllerBase
    {
        private readonly IUserSkillsService userSkillsService;
        public UserSkillsController(IUserSkillsService userSkills)
        {
            userSkillsService = userSkills;
        }

        [HttpGet]
        [Route("[action]")]
        [Route("api/UserSkills/GetUserSkills")]
        public IEnumerable<UserSkill> GetUserSkills()
        {
            return userSkillsService.GetUserSkills();
        }

        [HttpPost]
        [Route("[action]")]
        [Route("api/UserSkills/AddUserSkills")]
        public UserSkill AddUserSkills(UserSkill userSkills)
        {
            return userSkillsService.AddUserSkills(userSkills);
        }

        [HttpPut]
        [Route("[action]")]
        [Route("api/UserSkills/EditUserSkills")]
        public UserSkill EditUser(UserSkill userSkills)
        {
            return userSkillsService.UpdateUserSkills(userSkills);
        }

        [HttpDelete]
        [Route("[action]")]
        [Route("api/UserSkills/DeleteUserSkills")]
        public UserSkill DeleteUser(string Id)
        {
            return userSkillsService.DeleteUserSkills(Id);
        }

    }
}
