﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MITT_Leni_Nurliani.IServices;
using MITT_Leni_Nurliani.Models;
using System.Collections.Generic;

namespace MITT_Leni_Nurliani.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserSkillService userService;
        public UserController(IUserSkillService user)
        {
            userService = user;
        }

        [HttpGet]
        [Route("[action]")]
        [Route("api/User/GetUser")]
        public IEnumerable<User> GetUser()
        {
            return userService.GetUser();
        }

        [HttpPost]
        [Route("[action]")]
        [Route("api/User/AddUser")]
        public User AddUser(User user)
        {
            return userService.AddUser(user);
        }

        [HttpPut]
        [Route("[action]")]
        [Route("api/User/EditUser")]
        public User EditUser(User user)
        {
            return userService.UpdateUser(user);
        }

        [HttpDelete]
        [Route("[action]")]
        [Route("api/User/DeleteUser")]
        public User DeleteUser(string username)
        {
            return userService.DeleteUser(username);
        }

    }
}
