﻿using Microsoft.EntityFrameworkCore;
using MITT_Leni_Nurliani.IServices;
using MITT_Leni_Nurliani.Models;
using System.Collections.Generic;
using System.Linq;

namespace MITT_Leni_Nurliani.Services
{
    public class UserSkillsService : IUserSkillsService
    {
        db_mittContext dbContext;
        public UserSkillsService(db_mittContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public IEnumerable<UserSkill> GetUserSkills()
        {
            var userSkills = dbContext.UserSkills.ToList();
            return userSkills;
        }

        public UserSkill AddUserSkills(UserSkill userSkills)
        {
            if (userSkills != null)
            {
                dbContext.UserSkills.Add(userSkills);
                dbContext.SaveChanges();
                return userSkills;
            }
            return null;
        }

        public UserSkill UpdateUserSkills(UserSkill userSkill)
        {
            dbContext.Entry(userSkill).State = EntityState.Modified;
            dbContext.SaveChanges();
            return userSkill;
        }


        public UserSkill DeleteUserSkills(string Id)
        {
            var userSkill = dbContext.UserSkills.FirstOrDefault(o => o.UserSkillId == Id);
            dbContext.Entry(userSkill).State = EntityState.Deleted;
            dbContext.SaveChanges();
            return userSkill;
        }
    }
}
