﻿using Microsoft.EntityFrameworkCore;
using MITT_Leni_Nurliani.IServices;
using MITT_Leni_Nurliani.Models;
using System.Collections.Generic;
using System.Linq;

namespace MITT_Leni_Nurliani.Services
{
    public class UserService : IUserSkillService
    {
        db_mittContext dbContext;
        public UserService(db_mittContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public IEnumerable<User> GetUser()
        {
            var user = dbContext.Users.ToList();
            return user;
        }

        public User AddUser(User user)
        {
            if(user != null)
            {
                dbContext.Users.Add(user);
                dbContext.SaveChanges();
                return user;
            }
            return null;
        }

        public User UpdateUser(User user)
        {
            dbContext.Entry(user).State = EntityState.Modified;
            dbContext.SaveChanges();
            return user;
        }

        public User DeleteUser(string userName)
        {
            var user = dbContext.Users.FirstOrDefault(o => o.Username == userName);
            dbContext.Entry(user).State = EntityState.Deleted;
            dbContext.SaveChanges();
            return user;
        }
    }
}
