﻿using MITT_Leni_Nurliani.Models;
using System.Collections;
using System.Collections.Generic;

namespace MITT_Leni_Nurliani.IServices
{
    public interface IUserSkillService
    {
        IEnumerable<User> GetUser();
        User AddUser(User user);
        User UpdateUser(User user);
        User DeleteUser(string userName);
    }
}
