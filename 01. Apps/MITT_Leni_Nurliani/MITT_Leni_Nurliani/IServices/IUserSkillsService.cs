﻿using MITT_Leni_Nurliani.Models;
using System.Collections.Generic;

namespace MITT_Leni_Nurliani.IServices
{
    public interface IUserSkillsService
    {
        IEnumerable<UserSkill> GetUserSkills();
        UserSkill AddUserSkills(UserSkill userSkill);
        UserSkill UpdateUserSkills(UserSkill userSkill);
        UserSkill DeleteUserSkills(string Id);
    }
}
