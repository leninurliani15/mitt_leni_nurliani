﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MITT_Leni_Nurliani.Models
{
    public partial class Skill
    {
        public int SkillId { get; set; }
        public string SkillName { get; set; }
    }
}
