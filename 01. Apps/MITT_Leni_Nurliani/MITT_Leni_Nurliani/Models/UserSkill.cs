﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MITT_Leni_Nurliani.Models
{
    public partial class UserSkill
    {
        public string UserSkillId { get; set; }
        public string Username { get; set; }
        public int? SkillId { get; set; }
        public int? SkillLevelId { get; set; }
    }
}
