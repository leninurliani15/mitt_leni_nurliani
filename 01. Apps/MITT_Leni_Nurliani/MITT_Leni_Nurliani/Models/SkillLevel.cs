﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MITT_Leni_Nurliani.Models
{
    public partial class SkillLevel
    {
        public int SkillLevelId { get; set; }
        public string SkillLevelName { get; set; }
    }
}
