﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MITT_Leni_Nurliani.Models
{
    public partial class UserProfile
    {
        public int UserProfileId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime? Bod { get; set; }
        public string Email { get; set; }
    }
}
