﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MITT_Leni_Nurliani.Models
{
    public partial class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
