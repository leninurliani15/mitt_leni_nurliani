CREATE DATABASE db_mitt;

CREATE TABLE db_mitt.dbo.user_skills (
    userSkillID nvarchar(50) NOT NULL,
    username nvarchar(50) NULL,
    skillID int NULL,
    skillLevelID int NULL,
    CONSTRAINT userSkillID PRIMARY KEY (userSkillID)
);

CREATE TABLE db_mitt.dbo.[user] (
    username nvarchar(50) NOT NULL,
    [password] nvarchar(50) ,
    CONSTRAINT username PRIMARY KEY (username)
);

CREATE TABLE db_mitt.dbo.user_profile (
    userProfileID int NOT NULL,
    username nvarchar(50) NULL,
    name varchar(50) NULL,
	[address] nvarchar(500) NULL,
	bod date NULL,
	email nvarchar(50) NULL,
    CONSTRAINT userProfileID PRIMARY KEY (userProfileID)
);

CREATE TABLE db_mitt.dbo.skill_level (
    skillLevelID int NOT NULL,
    skillLevelName varchar(500) NULL,
    CONSTRAINT skillLevelID PRIMARY KEY (skillLevelID)
);

CREATE TABLE db_mitt.dbo.skill (
    skillID int NOT NULL,
    skillName varchar(500) NULL,
    CONSTRAINT skillID PRIMARY KEY (skillID)
);